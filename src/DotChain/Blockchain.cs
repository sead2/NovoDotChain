using DotChain.Extensions;
using DotChain.Models;

namespace DotChain;

public class Blockchain
{
    private List<Block> Chain { get; }
    public int Height { get; private set; }

    public Blockchain()
    {
        Chain = new List<Block>();
        Height = -1;
        Initialize();
    }

    /// <summary>
    /// This method will check for the height of the chain and if there isn't a Genesis Block it will create it.
    /// </summary>
    private void Initialize()
    {
        if (Height != -1) return;
        var block = new Block(new { data = "Genesis Block" });
        AddBlock(block);
    }

    /// <summary>
    /// It will store a block in the chain
    /// </summary>
    /// <param name="incomingBlock"></param>
    /// <returns>The method will return the block added or the errors happen during the execution.</returns>
    private (Block? block, string[] errors) AddBlock(Block incomingBlock)
    {
        try
        {
            var (isValid, errors) = ValidateChain();
            if (!isValid) return (null, errors);

            var block = incomingBlock with
            {
                Height = Chain.Count,
                Time = TimestampExtensions.GetCurrentTimestamp(),
                PreviousBlockHash = Height < 0 ? null : Chain[Height - 1].Hash
            };

            var newBlock = block with { Hash = block.ToSha256() };

            Chain.Add(newBlock);
            Height = Chain.Count;
            return (newBlock, Array.Empty<string>());
        }
        catch (Exception e)
        {
            return (null, new []{e.Message, e.StackTrace})!;
        }
    }

    /// <summary>
    /// The requestMessageOwnershipVerification(address) method
    /// will allow you  to request a message that you will use to
    /// sign it with your Bitcoin Wallet (Electrum or Bitcoin Core)
    /// This is the first step before submit your Block.
    /// </summary>
    /// <param name="address"></param>
    /// <returns>The method return the message to be signed </returns>
    public BlockMessage RequestMessageOwnershipVerification(string address) =>
        new (address, TimestampExtensions.GetCurrentTimestamp());

    ///  <summary>
    /// The submitStar(message, signature, item) method
    /// will allow users to register a new Block with the star object
    /// into the chain. This method will resolve with the Block added or
    /// reject with an error.
    ///  </summary>
    ///  <param name="message"></param>
    ///  <param name="signature"></param>
    ///  <param name="pubKey"></param>
    ///  <param name="star"></param>
    ///  <returns></returns>
    public (Block? block, string[] errors) SubmitStar(BlockMessage message, string signature, string pubKey, BlockStar star)
    {
        if (message.GetElapsedMinutes() >= 5)
            return (null, new[] { "Timeout" });

        if (!Wallet.Verify(message.Message, signature, pubKey))
            return (null, new[] { "The message with wallet address and signature are invalid" });

        var data = new BlockStarData
        {
            Owner = message.Address,
            BlockStar = star
        };

        var block = new Block(data);
        return AddBlock(block);
    }

    /// <summary>
    /// This method will return a Promise that will resolve with the Block object
    /// </summary>
    /// <param name="height"></param>
    /// <returns></returns>
    public Block? GetBlockByHeight(int height)
    {
        return Chain.SingleOrDefault(p => p.Height == height);
    }
    
    /// <summary>
    /// This method will return a Promise that will resolve with the Block
    /// </summary>
    /// <param name="hash"></param>
    /// <returns></returns>
    public Block? GetBlockByHash(string hash)
    {
        return Chain.SingleOrDefault(p => p.Hash == hash);
    }    
    
    /// <summary>
    /// This method will return a Promise that will resolve with an array of Stars objects existing in the chain
    /// and are belongs to the owner with the wallet address passed as parameter.
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    public BlockStar?[] GetStarsByWalletAddress(string address)
    {
        return Chain.Where(p => p.GetData<BlockStarData>()?.Owner == address)
            .Select(x => x.GetData<BlockStarData>()?.BlockStar)
            .ToArray();
    }
 
    /// <summary>
    /// This method will return a Promise that will resolve with the list of errors when validating the chain.
    /// </summary>
    /// <returns></returns>
    public (bool IsValid, string[] Errors) ValidateChain()
    {
        var errors = new List<string>();
        for (var i = 1; i < Chain.Count; i++)
        {
            var block = Chain[i];

            if (!block.Validate())
                errors.Add($"Block {block.Hash} in position {i} is broken!!!");

            if (block.PreviousBlockHash != Chain[i - 1].Hash)
                errors.Add($"Chain was corrupted: the block ${block.Hash} in position ${i} is broken!!!");
        }

        return (!errors.Any(), errors.ToArray());
    }
}