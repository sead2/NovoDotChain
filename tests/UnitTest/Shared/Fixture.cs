using System;
using System.Text.Json;
using System.Threading.Tasks;
using DotChain;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.Shared;

public class Fixture : IAsyncLifetime
{
    protected Stub Data { get; set; } = new("");
    protected Block Block { get; set; } = new(new { });
    protected Blockchain Blockchain { get; set; } = new();
    protected Wallet Wallet { get; set; } = new();
    protected ITestOutputHelper OutputHelper { get; }
    
    protected Fixture(ITestOutputHelper outputHelper)
    {
        OutputHelper = outputHelper;
    }
    
    public Task InitializeAsync()
    {
        Data = new Stub(Guid.NewGuid().ToString());
        Block = new Block(Data);

        return Task.CompletedTask;
    }
    
    public Task DisposeAsync()
    {
        return Task.CompletedTask;
    }    
}